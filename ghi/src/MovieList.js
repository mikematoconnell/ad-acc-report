import React from "react";
import { Link } from 'react-router-dom';

import CreateReview from "./Modal";



class MovieList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sampleMovies: []
        };
    }
    
    async componentDidMount() {
        const url = `${process.env.REACT_APP_LOCAL_HOST}monolith/movie_info/`;

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let movie of data.Movie_Info) {
                    const detailUrl = `${process.env.REACT_APP_LOCAL_HOST}monolith/movie_info/${movie.id}/`;
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests);
                const sampleMovies = [];
                
                for (const reviewResponse of responses) {
                    if (reviewResponse.ok) {
                        const details = await reviewResponse.json();
                        sampleMovies.push(details);
                    } else {
                        console.error(reviewResponse);
                    }
                }
                this.setState({sampleMovies: sampleMovies});
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
            <div className="bg-danger bg-gradient">
            <div className="px-4 py-5 my-5 mt-0 text-center">
            <h6 className="display-5 fw-bold">Adaptation Accuracy</h6>
                <img src="https://i.ibb.co/cJkH3nF/Untitled-Artwork.png" alt="" width="200" height="200" /> 
            <h6 className="display-5 fw-bold">Report!</h6>
            <div className="col-lg-6 mx-auto">
                <p className="lead mb-4">
                Find Movies and TV shows to review below!
                </p>
                <Link to="/how_to_review" className="btn btn-primary btn-lg px-4 gap-3">How to Write a Review</Link>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                </div>
            </div>
            </div>
            <div className="row-cols-1 g-4">
                {this.state.sampleMovies.map(movie => {
                    const url = "http://image.tmdb.org/t/p/original"
                    return (
                        // <div className="col">
                        
                            <div key={movie.id} className="card mb-3 w-50 mx-auto card-hover-class" divstyle={"max-width: 540px;"}><Link to={`/movie/${movie.id}`} style={{textDecoration: "none", color: "black"}}>
                                {/* <div className="bg-primary bg-gradient"> */}
                                <div className="shadow p-4 mt-4">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                        <img src={url + movie.movie_poster} className="img-fluid rounded-start" divstyle={"width:100px;height:200px;"} alt="https://i.ibb.co/cJkH3nF/Untitled-Artwork.png"></img>
                                    </div>
                                <div className="col-md-8">
                                    <div className="card-body" >
                                        <h5 className="card-title">
                                            <u>{movie.movie_name} </u>
                                        </h5>
                                        <p className="card-text">{movie.movie_synopsis}</p>
                                        <CreateReview movie={movie} />
                                        {/* <Link to="/create_review" className="btn btn-primary btn-lg px-4 gap-3">How to Write a Review</Link> */}
                                    </div>
                                </div>
                                </div>
                                
                                </div>
                                </Link>
                                {/* </div> */}
                            </div> 
                              
                        // </div>
                        
                    );
                })}
                </div>
                </div>
            </>
        )
}
}
export default MovieList;
